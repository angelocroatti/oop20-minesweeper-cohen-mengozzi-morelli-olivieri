package controllers;

import java.io.IOException;

/**interface for BackHomeController.*/
public interface BackHomeInterface {

    /**
     * The handler for 'HOME' button.
     * @exception IOException
     *                            if an I/O error occurs.
     */
    void btBackHome() throws IOException;
}
