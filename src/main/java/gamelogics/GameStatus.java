package gamelogics;

/**
 * The possible status of the game.
 */
public enum GameStatus {

    /**
     * The game is being played.
     */
    PLAYING,

    /**
     * The player won the game.
     */
    WON,

    /**
     * The player lost the game.
     */
    LOST
}
